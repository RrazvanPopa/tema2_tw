function checkAreTokensValid(tokens){
    for (var i = 0; i< tokens.length; i++ ) {
          
           if(typeof tokens[i].tokenName !== "string"){
               return false;
           }
    }
    
    return true;
}

function addTokens(input, tokens){
    if(typeof input !== "string") throw("Invalid input");
    if(input.length < 6) throw("Input should have at least 6 characters");
    if(!checkAreTokensValid(tokens)) throw ("Invalid array format");
    
    var toReplace="...";
    
    for (var i = tokens.length; i--; ) {
        input = input.replace(toReplace,"${"+tokens[i].tokenName+"}");
    }
    return input;
    
}

const app = {
    addTokens: addTokens
}

module.exports = app;